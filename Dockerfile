FROM php:8.2-fpm-alpine AS ext-amqp

ENV EXT_AMQP_VERSION=latest

RUN docker-php-source extract \
    && apk -Uu add git rabbitmq-c-dev \
    && git clone --branch $EXT_AMQP_VERSION --depth 1 https://github.com/php-amqp/php-amqp.git /usr/src/php/ext/amqp \
    && cd /usr/src/php/ext/amqp && git submodule update --init \
    && docker-php-ext-install amqp
    
RUN ls -al /usr/local/lib/php/extensions/

FROM php:8.2-fpm-alpine AS ext-redis

RUN apk add --no-cache --update --virtual buildDeps autoconf zip libzip-dev $PHPSIZE_DEPS \ 
    && mkdir -p /usr/src/php/ext/redis \
    && curl -fsSL https://pecl.php.net/get/redis --ipv4 | tar xvz -C "/usr/src/php/ext/redis" --strip 1 \
    && docker-php-ext-install redis \
    && echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini \
    && apk del buildDeps

RUN ls -al /usr/local/lib/php/extensions/*
 

FROM php:8.2-fpm-alpine

ENV COMPOSER_ALLOW_SUPERUSER 1 \
    PHP_OPCACHE_VALIDATE_TIMESTAMPS ${PHP_OPCACHE_VALIDATE_TIMESTAMPS:-0} \
    PHP_OPCACHE_MAX_ACCELERATED_FILES ${PHP_OPCACHE_MAX_ACCELERATED_FILES:-10000} \
    PHP_OPCACHE_MEMORY_CONSUMPTION ${PHP_OPCACHE_MEMORY_CONSUMPTION:-192} \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE ${PHP_OPCACHE_MAX_WASTED_PERCENTAGE:-10}

RUN apk --no-cache add tzdata git mc zip wget curl shadow bash musl musl-utils musl-locales rabbitmq-c

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions intl zip mysqli pdo_mysql memcached pcntl sockets gettext opcache

COPY --from=ext-amqp /usr/local/etc/php/conf.d/docker-php-ext-amqp.ini /usr/local/etc/php/conf.d/docker-php-ext-amqp.ini
COPY --from=ext-amqp /usr/local/lib/php/extensions/no-debug-non-zts-20220829/amqp.so /usr/local/lib/php/extensions/no-debug-non-zts-20220829/amqp.so

COPY --from=ext-redis /usr/local/etc/php/conf.d/redis.ini /usr/local/etc/php/conf.d/redis.ini
COPY --from=ext-redis /usr/local/lib/php/extensions/no-debug-non-zts-20220829/redis.so /usr/local/lib/php/extensions/no-debug-non-zts-20220829/redis.so

COPY symfony-cli_5.7.3_x86_64.apk /srv/symfony-cli_5.7.3_x86_64.apk

RUN set -xe \
    && apk add --no-cache --update --virtual .phpize-deps $PHPIZE_DEPS \
    && apk add --allow-untrusted /srv/symfony-cli_5.7.3_x86_64.apk \
    && rm -rf /srv/symfony-cli_5.7.3_x86_64.apk \
    && rm -rf /usr/share/php \
    && rm -rf /tmp/* \
    && apk del  .phpize-deps


COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

COPY conf.d/php-extend.ini /usr/local/etc/php/conf.d/php-extend.ini
COPY conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

ENV TZ UTC
ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8

EXPOSE 9000

WORKDIR "/srv/app"

